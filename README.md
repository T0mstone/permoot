# Permoot - A general-purpose permutation library
A [permutation](https://en.wikipedia.org/wiki/Permutation) is a mathematical object
describing a rearrangement of some (finite) number of objects.

As an example, sorting an array gives you a permutation on the array indices.
Inversely, you can use any permutation on `N` elements as a way to reorder an array of length `N`
(see [`Permutation::apply`])

# Crate features
By default, permoot is `no_std`.
No crate features are enabled by default.
- The `std` feature enables a few extra features that require the standard library
- The `rand` feature enables `rand` support, allowing you to generate random permutations

# Possible future features
I might add these if I come around to it:
- `serde` support