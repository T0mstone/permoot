#![cfg_attr(not(feature = "std"), no_std)]
#![cfg_attr(all(doc, rustc_channel = "nightly"), feature(doc_auto_cfg))]
#![doc = include_str!("../README.md")]
#![warn(missing_docs)]

pub use crate::sign::PermutationSign;
pub use crate::all_perms::all_permutations;

mod util;

/// Errors and related types
pub mod error;
mod sign;

/// The main type, representing a rearrangement of `N` objects.
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Permutation<const N: usize> {
	arr_repr: [usize; N],
}

mod math;
#[cfg(feature = "rand")]
mod rand_impl;
/// Different representations for a permutations
pub mod reprs;
/// Functionality related to sorting an array or a slice
pub mod sort;

mod all_perms;