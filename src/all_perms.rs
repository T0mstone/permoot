use crate::Permutation;
use crate::util::range_array;

/// Returns an iterator of all permutations of length `N`
///
/// The implementation uses [Heap's algorithm](https://en.wikipedia.org/wiki/Heap%27s_algorithm),
/// which means the order of the outputs is deterministic. This function guarantees that order as public API.
///
/// # Example
/// ```
/// # use crate::permoot::all_permutations;
/// // Convert the iterator into a single string of comma-separated permutations, where each one is represented by three letters
/// let all_perms_of_three = all_permutations::<3>().map(|p| p.to_array_repr().into_iter().map(|i| (i as u8 + b'A') as char).collect::<String>()).collect::<Vec<String>>().join(",");
/// assert_eq!(all_perms_of_three, "ABC,BAC,CAB,ACB,BCA,CBA");
/// ```
pub fn all_permutations<const N: usize>() -> impl Iterator<Item = Permutation<N>> {
	let mut stack = [0; N];
	let mut i = 1;

	core::iter::successors(Some(range_array()), move |prev| {
		let mut arr = [0; N];
		arr.copy_from_slice(prev.as_slice());

		loop {
			let curr = stack.get_mut(i)?;
			if *curr < i {
				if i % 2 == 0 {
					arr.swap(0, i);
				} else {
					arr.swap(*curr, i);
				}

				*curr += 1;
				i = 1;

				return Some(arr);
			} else {
				*curr = 0;
				i += 1;
			}
		}
	}).map(|arr| Permutation { arr_repr: arr })
}

#[cfg(test)]
mod tests {
	use bstr::ByteSlice;
	use super::*;

	fn test_control<const N: usize>(control: impl AsRef<[u8]>) {
		let spl = control.as_ref().split(|b| *b == b',');
		for (i, (p, cs)) in all_permutations::<N>().zip(spl).enumerate() {
			assert_eq!(cs.len(), N, "test error: invalid control bstring (invalid length of {:?})", cs.as_bstr());
			let mut arr = [0; N];
			arr.copy_from_slice(cs);
			let c = Permutation { arr_repr: arr.map(|c| (c - b'A') as usize) };

			assert_eq!(p, c, "permutation {i} not equal to control");
		}

	}

	#[test]
	fn all_two() {
		test_control::<2>(b"AB,BA");
	}

	#[test]
	fn all_three() {
		test_control::<3>(b"ABC,BAC,CAB,ACB,BCA,CBA");
	}

	#[test]
	fn all_four() {
		test_control::<4>(b"\
			ABCD,BACD,CABD,ACBD,BCAD,CBAD,\
			DBAC,BDAC,ADBC,DABC,BADC,ABDC,\
			ACDB,CADB,DACB,ADCB,CDAB,DCAB,\
			DCBA,CDBA,BDCA,DBCA,CBDA,BCDA");
	}
}