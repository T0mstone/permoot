use rand::distributions::{Distribution, Standard};
use rand::seq::SliceRandom;
use rand::Rng;

use crate::util::range_array;
use crate::Permutation;

impl<const N: usize> Distribution<Permutation<N>> for Standard {
	fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Permutation<N> {
		let mut arr_repr = range_array();

		arr_repr.shuffle(rng);

		Permutation { arr_repr }
	}
}
