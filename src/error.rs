/// An invalid array or function representation was encountered
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum InvalidArrFnRepr {
	/// The output (`value`) for the `input` is too large (should have been `<= max`)
	#[allow(missing_docs)]
	InvalidValue {
		input: usize,
		value: usize,
		max: usize,
	},
	/// The output (`value`) is associated to at least two different inputs
	#[allow(missing_docs)]
	RepeatedValue {
		first_input: usize,
		second_input: usize,
		value: usize,
	},
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
/// An location of any single value in [`Cycles`](crate::reprs::Cycles)
pub struct CycleElementLocation {
	/// The index of the cycle containing the value
	pub cycle_index: usize,
	/// The index of the value inside the cycle
	pub index: usize,
}

/// An invalid cycle representation was encountered
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum InvalidCyclesRepr {
	/// A too large `value` at `location` (should have been `< upper_bound`)
	#[allow(missing_docs)]
	InvalidValue {
		location: CycleElementLocation,
		value: usize,
		upper_bound: usize,
	},
	/// The `value` appears in multiple locations (either in multiple cycles or multiple times in a single cycle)
	#[allow(missing_docs)]
	RepeatedValue {
		first_occurrence: CycleElementLocation,
		second_occurrence: CycleElementLocation,
		value: usize,
	},
}

/// An invalid swap representation was encountered
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum InvalidSwaps {
	/// The left value of the swap at `index` is too large (should have been `< upper_bound`)
	#[allow(missing_docs)]
	InvalidLeftValue {
		index: usize,
		value: usize,
		upper_bound: usize,
	},
	/// The right value of the swap at `index` is too large (should have been `< upper_bound`)
	#[allow(missing_docs)]
	InvalidRightValue {
		index: usize,
		value: usize,
		upper_bound: usize,
	},
	/// The given list of swaps went over the maximum number of swaps for the type
	#[allow(missing_docs)]
	TooLong {
		max_length: usize
	}
}

/// Tried to use a slice longer than `self.max` in the context of a permutation on `self.max` objects
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
#[allow(missing_docs)]
pub struct TooLongSliceForPermutation {
	pub length: usize,
	pub max_length: usize,
}
