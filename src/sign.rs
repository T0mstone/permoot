/// A type representing the sign (parity) of a permutation ([wiki](https://en.wikipedia.org/wiki/Parity_of_a_permutation))
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
pub enum PermutationSign {
	/// The sign of an even permutation
	Positive,
	/// The sign of an odd permutation
	Negative,
}

#[allow(missing_docs)] // the function names say it all
impl PermutationSign {
	pub fn is_positive(&self) -> bool {
		matches!(self, Self::Positive)
	}

	pub fn is_negative(&self) -> bool {
		matches!(self, Self::Negative)
	}
}

macro_rules! impl_from_sign {
    ($p:expr, $n:expr; $($t:ty)*) => {
		$(
		impl From<PermutationSign> for $t {
			fn from(s: PermutationSign) -> Self {
				match s {
					PermutationSign::Positive => $p,
					PermutationSign::Negative => $n
				}
			}
		}
		)*
	};
}

impl_from_sign!(1, -1; i8 i16 i32 i64 i128 isize);
impl_from_sign!(1.0, -1.0; f32 f64);
